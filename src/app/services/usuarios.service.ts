import { Injectable } from '@angular/core';
import { UsuarioDataI } from '../components/interfaces/usuario.interfase';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  listUsuarios: UsuarioDataI[] = [
    {position: 123, nombre: 'Laura Rojas', fechaActual: "12/04", numTarjeta:111222333445},
    {position: 222, nombre: 'Maria Dolores', fechaActual: "05/01", numTarjeta:124578124578},
    {position: 345, nombre: 'Esteban Quito', fechaActual: "04/08", numTarjeta:369852146532},
    
  ];
  constructor() { }

  getUsuario():UsuarioDataI[]{
    return this.listUsuarios.slice();

  }

  eliminarUsuario(index:number){
    this.listUsuarios.splice(index,1);
  }

  agregarUsuario(usuario:UsuarioDataI) {
    this.listUsuarios.unshift(usuario);
  }
}
