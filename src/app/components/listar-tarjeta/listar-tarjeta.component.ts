import { Component, OnInit } from '@angular/core';
import { UsuarioDataI } from '../interfaces/usuario.interfase';
import { MatTableDataSource } from '@angular/material/table';
import { UsuarioService } from 'src/app/services/usuarios.service';
import { MatSnackBar } from '@angular/material/snack-bar';




@Component({
  selector: 'app-listar-tarjeta',
  templateUrl: './listar-tarjeta.component.html',
  styleUrls: ['./listar-tarjeta.component.css']
})

export class ListarTarjetaComponent implements OnInit {

  listUsuarios : UsuarioDataI[]=[];
  
  displayedColumns: string[] = ['position', 'nombre', 'fechaActual', 'numTarjeta', 'acciones'];
  dataSource!: MatTableDataSource<any>;



  constructor(private _usuarioService: UsuarioService,
              private _snackbar:MatSnackBar) { }

  ngOnInit(): void {
    this.cargarUsuarios();
  
  }

  cargarUsuarios(){
    this.listUsuarios = this._usuarioService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listUsuarios);
  }

  eliminarUsuario(index:number){

    const opcion = confirm('Estas seguro de eliminar el usuario');

    if (opcion) {
      
      console.log(index);
    this._usuarioService.eliminarUsuario(index);
    this.cargarUsuarios();
    this._snackbar.open('El usuario fue eliminado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
     });
    }   
  }


  modificarUsuario(usuario:string){
    console.log(usuario);
    

  }

}
