import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuarios.service';
import { UsuarioDataI } from '../interfaces/usuario.interfase';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-crear-tarjeta',
  templateUrl: './crear-tarjeta.component.html',
  styleUrls: ['./crear-tarjeta.component.css']
})
export class CrearTarjetaComponent implements OnInit {
  hide = true;

  form!:FormGroup
  constructor(private fb:FormBuilder,
              private _usuarioservice:UsuarioService,
              private _snackbar: MatSnackBar) {
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  crearFormulario():void{
    this.form = this.fb.group({
      nombre:['',Validators.required],
      numTarjeta:['',
      [Validators.required,Validators.minLength(12),Validators.maxLength(12),Validators.pattern(/^4[0-9]{12}(?:[0-9]{3})?$/)]],
      fechaActual:['',[Validators.required,Validators.pattern(/^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/)]],
      position:['',[Validators.required,Validators.minLength(3),Validators.maxLength(3)]],
    })
  }


  crearTarjeta(){
    const user : UsuarioDataI = {
      nombre: this.form.value.nombre,
      position: this.form.value.position,
      numTarjeta: this.form.value.numTarjeta,
      fechaActual: this.form.value.fechaActual,
    }

    this._usuarioservice.agregarUsuario(user);
    console.log(this.form.value);

    this._snackbar.open('El usuario fue agregado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
     });
      
    }
    
   
    
  
} 
